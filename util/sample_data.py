"""Generate sample data

1) Set up
- a world grid (~ the head mesh) in 2d or 3d
- multiple electrical fields (~condition) with an origin and intensities, and linear damping by distance
- a functional relationship (sigmoidal relationship) at given point(s) in the grid an the output variable

2) Calc output for all fields (= condition*intensities)

3) Do single element regression and get r^2 for each

4) Compare calculated hotspot with given on


"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.special.cython_special import expit
from sklearn.linear_model import Ridge


class grid:
    def __init__(self, xn, xy, xz=1):
        self.grid = np.zeros((xn, xy, xz, 1))  # for each new field/intensity, a new grid is appended
        self.damp = 10  # damping factor for the field
        self.n_fields = 0
        self.grid_shape = (xn, xy, xz)

        # init a grid with the functional relationships set to 0
        self.function_grid = np.empty_like(self.grid[:, :, :, 0], np.dtype(object))
        self.function_grid[:] = lambda _: 0  # you can now call self.function_grid[0,0,0]() = 0

        # for reasonable output behaviour, set maximum output value
        self.maxAmp = 10
        self.maxE = 0  # this is the maximum E in the whole self.grid, to normalize the input activation function

        self.output = None  # mep amplitude will be stored here
        self.res = np.zeros(self.grid_shape[:3])  # r^2 will be stored here

    def dist(self, a, b):
        """Get distance between a and b"""
        return np.linalg.norm(a - b)

    def create_field(self, x, y, z=1, intens=100):
        """
        Create sample e-field.
        This implementation is quite slow and should be vectorized.
        """
        # append to self.grid
        if self.n_fields > 0:
            self.grid = np.append(self.grid, np.zeros(self.grid_shape + (1,)), axis=3)

        for i in np.ndenumerate(self.grid):
            # iterate over all elements, and compute field value, depending on distance, original intensity and damping
            self.grid[i[0][:-1]][self.n_fields] = np.max((0,  # don't want negative values
                                                          intens - self.damp * self.dist(i[0][:-1],
                                                                                         np.array((x, y, z)))))
        self.n_fields += 1
        self.maxE = np.max(self.grid)

    def sig_activation(self, x):
        """Sigmoidal activation function.
        This uses the global field maximum to create sigmoidal activations.
        """
        # [-5,5] is reasonable range for exp() to go from 0 -> 1
        x /= self.maxE
        x = (x * 10) - 5
        # weight maxAmp with sigmoidal
        return expit(float(x)) * self.maxAmp

    def gen_additive_output(self):
        """
        For each element, sum up all outputs over all field.
        As the function_grid is init with ()->0, only the cells with sig_activation will produce an output.
        """
        result = []
        for cond in range(self.n_fields):
            output = 0
            for i in np.ndenumerate(self.function_grid):
                # call each element in function_grid
                output += i[1](self.grid[i[0] + (cond,)])
            result += [output]
        self.output = result
        return result

    def single_elm_regression(self):
        """
        Do regression for each element, calc r^2 and store in self.res
        """
        reg = Ridge()

        for elm in np.ndindex(self.grid_shape):
            reg.fit(self.grid[elm].reshape(-1, 1), self.output)
            self.res[elm] = reg.score(self.grid[elm].reshape(-1, 1), self.output)


# create world in 2D or 3D.
a = grid(10, 20)
# a = grid(10, 20,1) # 3d is possible as well


# add electric fields / conditions. give center position of e-field
condition_list = [[0, 1, 0],
                  [5, 10, 0],
                  [9, 20, 4],
                  [2, 2, 0],
                  [8, 2, 2]
                  ]

for cond in condition_list:
    print("Adding condition")
    # Create multiple fields at each site, with various intensities
    for intens in range(100, 120):
        a.create_field(x=cond[0],
                       y=cond[1],
                       z=cond[2], intens=intens)

# define elements which have a functional relationship to the output variable
hotspot = (4, 9, 0)
a.function_grid[hotspot] = a.sig_activation
# by default, all other elemnts have no relationship with output

# compute output (MEP amplitude) for each condition/zap.
a.gen_additive_output()

# do one regression for each element in the grid and save r^2 in a.res
a.single_elm_regression()

# get winner elm
best_elm = np.where(a.res == np.max(a.res))
best_elm = (best_elm[0][0], best_elm[1][0], best_elm[2][0])

print("Real hotspot: {}. Max r^2 found at: {}".format(hotspot, best_elm))

# some plotting
fig, ax = plt.subplots()
sc1 = ax.scatter(a.grid[hotspot[0],
                 hotspot[1],
                 hotspot[2], :],
                 a.output,
                 label='real hotspot score: {:1.4}'.format(a.res[hotspot[0],
                                                                 hotspot[1],
                                                                 hotspot[2]]),
                 s=2)
sc2 = ax.scatter(a.grid[best_elm[0],
                 best_elm[1],
                 best_elm[2], :],
                 a.output,
                 label='best hotspot score: {:1.4}'.format(a.res[best_elm[0],
                                                                 best_elm[1],
                                                                 best_elm[2]]),
                 s=2)
plt.xlabel("field value in elm")
plt.ylabel("output value (e.g. MEP)")
ax.legend()
plt.show()
print()
