Installation of Biosig 1.9.5. for Python 3.7 (Linux)
=================================================================

Extract: biosig source files:
-----------------------------------------------------------------
	cd .../pynibs/pynibs/pckg/biosig
	tar -xvf biosig4c++-1.9.5.src_fixed.tar.gz

cd into biosig folder and compile biosig:
-----------------------------------------------------------------
	cd biosig4c++-1.9.5
	./configure
	make

cd into biosig/python folder and execute setup.py:
-----------------------------------------------------------------
	cd python
	python setup.py install

add "usr/local/lib" folder to $LD_LIBRARY_PATH in ~/.bashrc
-----------------------------------------------------------------
	gedit ~/.bashrc
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/.local/lib

