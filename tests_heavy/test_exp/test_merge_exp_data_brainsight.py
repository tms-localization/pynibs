#!/usr/bin/python3

import unittest
import tempfile
import os
import subprocess
import pandas as pd
import numpy as np
import shutil

import pynibs

from icecream import ic

import pynibs.exp.brainsight


class TestMergeExpDataBrainsight(unittest.TestCase):

    def __init__(self, arg):
        super().__init__(arg)

        script_dir = os.path.dirname(
            os.path.realpath(__file__)
        )
        test_base_dir = os.path.dirname(script_dir)
        testdata_dir = os.path.join(test_base_dir,"data")
        
        self.testcase_sn4 = os.path.join(testdata_dir,"testsub.sn4")


    def test_merge_exp_data_brainsight_simnibs4(self):
        with tempfile.TemporaryDirectory() as tempdir:
            ###### preparation
            testcase_sn4_name = os.path.basename(self.testcase_sn4)
    
            # copy sample directories to temporary diretory
            shutil.copytree(self.testcase_sn4, os.path.join(tempdir, testcase_sn4_name))       
            
            testcase_sn4          = os.path.join(tempdir, testcase_sn4_name)
            create_subject_sn4_fn = os.path.join(testcase_sn4,"create_subject_testsub.sn4.py")

            # prepare subject object of testcases (to adapt to the new paths).
            # We need to switch to the directory of the 'create_subject'-script
            # for its proper execution (some paths are relative in that script).
            current_working_dir = os.getcwd()
            os.chdir( testcase_sn4 )
            with open( create_subject_sn4_fn ) as script:
                exec(script.read())
            os.chdir( current_working_dir )

            ###### run the to-be tested function
            subject_object_fn = os.path.join(testcase_sn4,"testsub.sn4.hdf5")
            subject = pynibs.load_subject( subject_object_fn )    
    
            pynibs.exp.brainsight.merge_exp_data(
                subject=subject,
                exp_idx="TMS_brainsight",
                mesh_idx="charm_refm1_sm",
                coil_outlier_corr_cond=True,
                remove_coil_skin_distance_outlier=True,
                coil_distance_corr=True,
                verbose=True,
                plot=True
            )
        
            ####### checks
            # content of experiment.hdf5
            exp_fn = os.path.join(testcase_sn4,"exp","roboTMS_offline_random","experiment.hdf5")
            phys_exp_data_raw = pd.read_hdf( exp_fn, "phys_data/raw/EMG" )
            phys_exp_data_postproc = pd.read_hdf( exp_fn, "phys_data/postproc/EMG" )
            stim_exp_data = pd.read_hdf( exp_fn, "stim_data" )

            def check( field, expected, frame ):
                assert all( entry == expected for entry in frame[field]), f"'{field}' should be {expected} for all pulses" 
            
            # dataframe: raw data
            field = "EMG Start"; expected = -100
            check( field, expected, phys_exp_data_raw )
            field = "EMG End"; expected = 100
            check( field, expected, phys_exp_data_raw )
            field = "EMG Res."; expected = 0.333333333
            check( field, expected, phys_exp_data_raw )
            field = "EMG Window Start"; expected = 10.0
            check( field, expected, phys_exp_data_raw )
            field = "EMG Window End"; expected = 90.0
            check( field, expected, phys_exp_data_raw )
            field = "EMG Channels"; expected = "2"
            check( field, expected, phys_exp_data_raw )
     
            # note: by checking the entire dataframe the above is redundant.
            #       I left it in here as a reference and example.
            exp_REF_fn = os.path.join(testcase_sn4,"exp","roboTMS_offline_random","experiment_REF.hdf5")
            phys_exp_data_raw_REF = pd.read_hdf( exp_REF_fn, "phys_data/raw/EMG" )
            assert phys_exp_data_raw.round(1).equals(phys_exp_data_raw_REF.round(1)), "Raw data is not the same as the reference data."
            
            # dataframe: postprocessed data
            exp_REF_fn = os.path.join(testcase_sn4,"exp","roboTMS_offline_random","experiment_REF.hdf5")
            phys_exp_data_postproc_REF = pd.read_hdf( exp_REF_fn, "phys_data/postproc/EMG" )
            
            if not phys_exp_data_postproc.round(1).equals(phys_exp_data_postproc_REF.round(1)):
                input("Pause....")

            assert phys_exp_data_postproc.round(1).equals(phys_exp_data_postproc_REF.round(1)), "Postprocessing data is not the same as the reference data."

            # dataframe: stimulation data
            stim_exp_data_REF = pd.read_hdf( exp_REF_fn, "stim_data" )
            assert stim_exp_data.round(1).equals(stim_exp_data_REF.round(1)), "Stimulation data is not the same as the reference data."

            # MEP plots
            for i in range(400):
                assert os.path.exists(os.path.join(testcase_sn4,"exp","roboTMS_offline_random","plots", "1", f"mep_{i:04d}.png")), "MEP plots for channel 1 should have been created."
                assert os.path.exists(os.path.join(testcase_sn4,"exp","roboTMS_offline_random","plots", "2", f"mep_{i:04d}.png")), "MEP plots for channel 2 should have been created."

            # other files 
            assert os.path.exists(os.path.join(testcase_sn4,"exp","roboTMS_offline_random","nnav2simnibs","mesh_charm_refm1_sm","testsub.sn4_T1_RAS.nii.gz")), "SimNIBS registered version of the T1 image should have been created."
            assert os.path.exists(os.path.join(testcase_sn4,"exp","roboTMS_offline_random","distance_histogram_orig.png")), "Coil distance historgram 'distance_histogram_orig.png' should have been created."
            assert os.path.exists(os.path.join(testcase_sn4,"exp","roboTMS_offline_random","distance_histogram_zm.png")), "Coil distance historgram 'distance_histogram_zm.png' should have been created."
        

if __name__ == '__main__':
    unittest.main()

