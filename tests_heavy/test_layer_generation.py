import unittest
import os
import copy
import simnibs
import pynibs


class TestRoi(unittest.TestCase):
    def __init__(self, arg):
        super().__init__(arg)

        subj_mesh_dir = './data/'
        subj_mesh_msh = os.path.join(subj_mesh_dir, "subject_0.msh")
        subj_mesh_hdf = os.path.join(subj_mesh_dir, "subject_0.hdf5")

        self.head_mesh = simnibs.mesh_io.read_msh(subj_mesh_msh)
        self.roi = pynibs.load_roi_surface_obj_from_hdf5(subj_mesh_hdf)["midlayer_m1s1pmd"]

    def test_generate_cortical_laminae(self):
        head_mesh = copy.deepcopy(self.head_mesh)
        roi = self.roi

        roi.generate_cortical_laminae(head_mesh)

        assert len(roi.layers) == 5, "Unexpected number of layers of layer creation."
        # check number of generates points and triangles per layer

    def test_load_roi_surface_obj_from_hdf5(self):
        pass


class TestHDF5io(unittest.TestCase):
    def __init__(self, arg):
        super().__init__(arg)

    def test_hdf5_io(self):
        pass


class TestCorticalLaminae(unittest.TestCase):
    def __init__(self, arg):
        super().__init__(arg)

    def test_generate_layer(self):
        # signature of the to-be tested function 'generate_layer(self, depth, roi)'
        pass
