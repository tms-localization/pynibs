pynibs.congruence package
=========================

Submodules
----------

pynibs.congruence.congruence module
-----------------------------------

.. automodule:: pynibs.congruence.congruence
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.congruence.ext\_metrics module
-------------------------------------

.. automodule:: pynibs.congruence.ext_metrics
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.congruence.stimulation\_threshold module
-----------------------------------------------

.. automodule:: pynibs.congruence.stimulation_threshold
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pynibs.congruence
   :members:
   :undoc-members:
   :show-inheritance:
