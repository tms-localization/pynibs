pynibs.regression package
=========================

Submodules
----------

pynibs.regression.regression module
-----------------------------------

.. automodule:: pynibs.regression.regression
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.regression.score\_types module
-------------------------------------

.. automodule:: pynibs.regression.score_types
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pynibs.regression
   :members:
   :undoc-members:
   :show-inheritance:
