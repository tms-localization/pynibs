pynibs package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 1

   pynibs.congruence
   pynibs.expio
   pynibs.mesh
   pynibs.models
   pynibs.regression
   pynibs.util

Submodules
----------

pynibs.coil module
------------------

.. automodule:: pynibs.coil
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.freesurfer module
------------------------

.. automodule:: pynibs.freesurfer
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.hdf5\_io module
----------------------

.. automodule:: pynibs.hdf5_io
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.muap module
------------------

.. automodule:: pynibs.muap
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.neuron module
--------------------

.. automodule:: pynibs.neuron
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.opt module
-----------------

.. automodule:: pynibs.opt
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.para module
------------------

.. automodule:: pynibs.para
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.roi module
-----------------

.. automodule:: pynibs.roi
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.subject module
---------------------

.. automodule:: pynibs.subject
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.tensor\_scaling module
-----------------------------

.. automodule:: pynibs.tensor_scaling
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.tms\_pulse module
------------------------

.. automodule:: pynibs.tms_pulse
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pynibs
   :members:
   :undoc-members:
   :show-inheritance:
