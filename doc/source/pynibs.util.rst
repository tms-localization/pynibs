pynibs.util package
===================

Submodules
----------

pynibs.util.quality\_measures module
------------------------------------

.. automodule:: pynibs.util.quality_measures
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.util.simnibs module
--------------------------

.. automodule:: pynibs.util.simnibs
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.util.util module
-----------------------

.. automodule:: pynibs.util.util
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pynibs.util
   :members:
   :undoc-members:
   :show-inheritance:
