pynibs.mesh package
===================

Submodules
----------

pynibs.mesh.mesh\_struct module
-------------------------------

.. automodule:: pynibs.mesh.mesh_struct
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.mesh.transformations module
----------------------------------

.. automodule:: pynibs.mesh.transformations
   :members:
   :undoc-members:
   :show-inheritance:

pynibs.mesh.utils module
------------------------

.. automodule:: pynibs.mesh.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pynibs.mesh
   :members:
   :undoc-members:
   :show-inheritance:
