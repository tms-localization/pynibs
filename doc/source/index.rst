:orphan:

The pyNIBS documentation.
=========================
**pyNIBS** is a Pyhon package to work with non-invasive brain stimulation (NIBS) data, foremost from transcranial magnetic stimulation (TMS) experiments.

We created this package to allow the mapping of causal structure-function relationships with TMS. A lot of analyses is based on calculations of the induced electric fields (e-fields) -- for this we rely on `SimNIBS <https://simnibs.github.io/simnibs/>`_.

Install **pyNIBS** via `pip <https://pypi.org/project/pynibs/>`_:

.. code-block:: bash

    pip install pynibs

or directly grab from our repository at `gitlab.gwdg.de/tms-localization/pynibs <https://gitlab.gwdg.de/tms-localization/pynibs>`_



Contents.
=========
.. toctree::
   :maxdepth: 1

   pynibs
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


References.
===========
- Weise, K., Numssen, O., Thielscher, A., Hartwigsen, G., & Knösche, T. R. (2020). A novel approach to localize cortical TMS effects. Neuroimage, 209, 116486. doi: `10.1016/j.neuroimage.2019.116486 <https://doi.org/10.1016/j.neuroimage.2019.116486>`_)
- Numssen, O., Zier, A. L., Thielscher, A., Hartwigsen, G., Knösche, T. R., & Weise, K. (2021). Efficient high-resolution TMS mapping of the human motor cortex by nonlinear regression. NeuroImage, 245, 118654. doi:`10.1016/j.neuroimage.2021.118654 <https://doi.org/10.1016/j.neuroimage.2021.118654>`_)
- Weise, K., Numssen, O., Kalloch, B., Zier, A. L., Thielscher, A., Hartwigsen, G., Knösche, T. R. (2022). Precise transcranial magnetic stimulation motor-mapping. Nature Protocols (in press).  doi: `10.1038/s41596-022-00776-6 <https://doi.org/10.1038/s41596-022-00776-6>`_
