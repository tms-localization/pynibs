Paraview 5.8 (64 bit), Ubuntu 16.04 (64 bit), Python 3.7
========================================================
Activate conda environment, where paraview is going to be installed
> conda activate MY_ENV

Download and install Paraview in your conda environment
>  conda install -c conda-forge paraview

Test your installation

> python
>>> from paraview.simple import *

Please also test if h5py still works
>>> import h5py

If Python crashes, try to reinstall h5py
> pip uninstall h5py
> pip install h5py

Open Python again and try to import h5py (h5py is shipped with paraview and may crash if two versions are installed at the same time)
> python
>>> from paraview.simple import *
>>> import h5py














Tested under Windows 10 (64 bit) with Paraview 4.2 (64 bit)!
=================================================================
Add the folowing paths to "PYTHONPATH" variable:

C:\Program Files (x86)\ParaView 4.2.0\bin
C:\Program Files (x86)\ParaView 4.2.0\lib
C:\Program Files (x86)\ParaView 4.2.0\lib\paraview-4.2\site-packages
C:\Program Files (x86)\ParaView 4.2.0\lib\paraview-4.2\site-packages\vtk

Add the folowing paths to "PATH" and "Path" variables:

C:\Program Files (x86)\ParaView 4.2.0\bin
C:\Program Files (x86)\ParaView 4.2.0\bin\Lib



Tested under Windows 10 (64 bit) with Paraview 5.1.2 (64 bit)!
=================================================================
Add the folowing paths to "PYTHONPATH" variable:
C:\Program Files\ParaView 5.1.2\bin
C:\Program Files\ParaView 5.1.2\bin\Lib
C:\Program Files\ParaView 5.1.2\lib\paraview-5.1\site-packages
C:\Program Files\ParaView 5.1.2\lib\paraview-5.1\site-packages\vtk

Add the folowing paths to "PATH" and "Path" variables:

C:\Program Files\ParaView 5.1.2\bin
C:\Program Files\ParaView 5.1.2\bin\Lib



Recommended by ParaView Wiki:
=================================================================
Add the folowing paths to "PYTHONPATH" variable:

C:\Program Files\ParaView 5.1.2\lib
C:\Program Files\ParaView 5.1.2\lib\paraview-5.1\site-packages
+ (vtkCommonCorePython not found Error)
C:\Program Files\ParaView 5.1.2\lib\paraview-5.1\site-packages\vtk
+ (DLLs not found)
C:\Program Files\ParaView 5.1.2\bin

Add the folowing paths to "PATH" and "Path" variables:

C:\Program Files\ParaView 5.1.2\lib



Tested under Windows 10 (64 bit) with Paraview 5.4.1 (64 bit)!
=================================================================
Add the folowing paths to "PYTHONPATH" variable:
C:\Program Files\ParaView 5.4.1-Qt5-OpenGL2-MPI-Windows-64bit\bin
C:\Program Files\ParaView 5.4.1-Qt5-OpenGL2-MPI-Windows-64bit\bin\Lib
C:\Program Files\ParaView 5.4.1-Qt5-OpenGL2-MPI-Windows-64bit\bin\Lib\site-packages
C:\Program Files\ParaView 5.4.1-Qt5-OpenGL2-MPI-Windows-64bit\bin\Lib\site-packages\vtk

Add the folowing paths to "PATH" and "Path" variables:

C:\Program Files\ParaView 5.4.1-Qt5-OpenGL2-MPI-Windows-64bit\bin
C:\Program Files\ParaView 5.4.1-Qt5-OpenGL2-MPI-Windows-64bit\bin\Lib
+
C:\Program Files\ParaView 5.4.1-Qt5-OpenGL2-MPI-Windows-64bit\bin\Lib\site-packages\paraview\vtk

!!! ATTENTION !!!
!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!
pip is not working afterwards and h5py package can also not be imported afterwards !!!
