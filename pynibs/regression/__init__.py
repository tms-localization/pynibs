"""
This holds methods for the TMS-based cortical localization approach as published in [1]_

References
----------
.. [1] Numssen, O., Zier, A. L., Thielscher, A., Hartwigsen, G., Knösche, T. R., & Weise, K. (2021).
   Efficient high-resolution TMS mapping of the human motor cortex by nonlinear regression. NeuroImage, 245, 118654.
"""
from .regression import *
from .score_types import *
from .dual_node_detection import *
