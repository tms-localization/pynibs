import pynibs
import numpy as np


def DI_wave(t, intensity, t0=5, dt=1.4, width=0.25):
    """
    Determines cortical DI waves from TMS

    Parameters
    ----------
    t: np.ndarray of float
        (n_t) Time axis in ms.
    intensity: float
        Stimulator intensity w.r.t resting motor threshold (typical range: [0 ... 2]).
    t0: float
        Offset time.
    dt: float
        Spacing of waves in ms.
    width: float
        Width of waves.

    Returns
    -------
    y: np.ndarray of float
        (n_t) DI waves.
    """
    waves = ["D", "I1", "I2", "I3", "I4"]

    x0 = dict()
    x0["D"] = 1.6952640144480995
    x0["I1"] = 1.314432218728424
    x0["I2"] = 1.4421623825084195
    x0["I3"] = 1.31643163560532
    x0["I4"] = 1.747079479469914

    amp = dict()
    amp["D"] = 12.83042571812661 / 35.46534715796085
    amp["I1"] = 35.46534715796085 / 35.46534715796085
    amp["I2"] = 26.15109003222628 / 35.46534715796085
    amp["I3"] = 15.491215097559184 / 35.46534715796085
    amp["I4"] = 10.461195366965226 / 35.46534715796085

    r = dict()
    r["D"] = 13.945868670402973
    r["I1"] = 8.707029476168504
    r["I2"] = 7.02266347578131
    r["I3"] = 16.74855628350182
    r["I4"] = 17.85806255278076

    y = np.zeros(len(t), dtype=np.float128)

    for i, w in enumerate(waves):
        y_ = np.exp(-(t - t0 - i * dt) ** 2 / (2 * width ** 2))
        y_ = y_ / np.max(y_)
        y_ = y_ * pynibs.expio.fit_funs.sigmoid(intensity, amp=amp[w], r=r[w], x0=x0[w])
        y = y + y_

    return y
