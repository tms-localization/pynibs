import unittest
import numpy as np
import pynibs
try:
    import simnibs
except ImportError:
    raise unittest.SkipTest("SimNIBS not found. Skipping tests in 'test_simnibs.py'.")


class TestSimnibs(unittest.TestCase):
    def __init__(self, arg):
        super().__init__(arg)

        # From:
        # "surf_nodes, tris = make_sphere_mesh()"
        # delaunay triangulation entails some random processes,
        # so we just pick one particular triangulation obtained by
        # the above-mentioned function here.
        surf_nodes = np.array([
            [0., 1., 0.],
            [-0.46346536, 0.77777778, 0.42457224],
            [0.07269269, 0.55555556, -0.82829572],
            [0.57364166, 0.33333333, 0.74821396],
            [-0.97861613, 0.11111111, -0.17310341],
            [0.83853075, -0.11111111, -0.53340463],
            [-0.24475729, -0.33333333, 0.9104849],
            [-0.38323471, -0.55555556, -0.7378951],
            [0.59040041, -0.77777778, 0.21561328],
            [-0., -1., 0.]
        ])
        tris = np.array([
            [1, 6, 4],
            [9, 6, 4],
            [8, 9, 6],
            [3, 1, 0],
            [2, 0, 4],
            [2, 0, 5],
            [7, 2, 4],
            [7, 9, 5],
            [8, 9, 5],
            [1, 0, 4],
            [3, 1, 6],
            [8, 3, 6],
            [3, 0, 5],
            [8, 3, 5],
            [7, 2, 5],
            [7, 9, 4]
        ])
        # From:
        # vol_nodes, tets = make_volmesh_box(size=5)
        # delaunay triangulation entails some random processes,
        # so we just pick one particular triangulation obtained by
        # the above-mentioned function here.
        vol_nodes = np.array([
            [-2.5, -2.5, -2.5],
            [-2.5, -2.5, 2.5],
            [2.5, -2.5, -2.5],
            [2.5, -2.5, 2.5],
            [-2.5, 2.5, -2.5],
            [-2.5, 2.5, 2.5],
            [2.5, 2.5, -2.5],
            [2.5, 2.5, 2.5]
        ])
        tets = np.array([
            [5, 4, 2, 0],
            [5, 1, 2, 0],
            [5, 3, 7, 2],
            [5, 6, 7, 2],
            [5, 6, 4, 2],
            [5, 3, 1, 2]
        ])

        self.volmesh = simnibs.Msh(
                nodes=simnibs.Nodes(node_coord=vol_nodes),
                elements=simnibs.Elements(tetrahedra=tets + 1)
        )
        # from:
        # "np.linspace(1,100,vol_nodes.shape[0])"
        # and
        # "np.linspace(1,50,tets.shape[0])"
        # result of 'numpy.random.default_rng(seed=42).random((3,8)) * 7'
        node_data_vector = np.array([
            [5.44868448, 1.36247095, 3.26704703, 0.30662636, 1.08002644, 4.78134267, 5.21333509, 6.77256813],
            [2.28077751, 2.59321794, 3.28689068, 1.32629951, 0.90945054, 3.32993448, 1.58836544, 4.68869796],
            [3.06006343, 5.82874737, 4.90185571, 2.18656649, 5.82581861, 5.6333505, 2.71234865, 2.01829673]
        ])
        # result of 'nupy.linalg.norm(node_data_vector, axis=1)'
        node_data_scalar = np.array(
                [6.65237528, 6.52345018, 6.74577172, 2.57568692, 5.99447411, 8.10458754, 6.08757774, 8.48086607]
        )
        # result of
        node_data_vector = np.transpose(node_data_vector)
        self.volmesh.add_node_field(node_data_scalar, 'magnE')
        self.volmesh.add_node_field(node_data_vector, 'E')

        self.midlayer_surf = simnibs.Msh(
                nodes=simnibs.Nodes(node_coord=surf_nodes),
                elements=simnibs.Elements(triangles=tris + 1)
        )

        self.gm_surf = simnibs.Msh(
                nodes=simnibs.Nodes(node_coord=surf_nodes * 2),
                elements=simnibs.Elements(triangles=tris + 1)
        )

        self.wm_surf = simnibs.Msh(
                nodes=simnibs.Nodes(node_coord=surf_nodes * 0.75),
                elements=simnibs.Elements(triangles=tris + 1)
        )

        # Debugging
        # self.volmesh.write("/tmp/test.msh")
        # self.midlayer_surf.write("/tmp/roi.msh")
        # self.gm_surf.write("/tmp/gm.msh")
        # self.wm_surf.write("/tmp/wm.msh")

        self.reference_solutions = {
            "gradient_gm_wm": np.array([
                [-1.71490311, 3.46308941, -8.82072504, -3.00075141,
                 0.84031719, -5.02211539, -11.57347333, -4.87389571,
                 -13.95101122, 4.41714376]
            ]),
            "efield_angle_theta": np.array([
                121.10500193, 121.75432648, 119.07643903, 123.42210583,
                117.18796886, 118.06182965, 118.64773481, 121.10111135,
                120.30784089, 118.58214421, 122.50287522, 122.73573535,
                124.20510611, 123.2927049, 118.03868137, 121.01344853])
        }

    def test_e_field_gradient_between_wm_gm(self):
        # perform 100 computations and compare their mean gain st the reference solution as
        # the involved 'interpolate_scattered' of SimNIBS produces non-consistent results.
        # (ie. results with some randomness, which becomes worse the coarser the mesh is)
        gradients_computed = np.zeros((1000, 10))

        for i in range(1000):
            gradients_computed[i, :] = pynibs.e_field_gradient_between_wm_gm(
                    roi_surf=self.midlayer_surf,
                    mesh=self.volmesh,
                    gm_nodes=self.gm_surf.nodes.node_coord,
                    wm_nodes=self.wm_surf.nodes.node_coord,
                    gm_center_distance=np.ones(self.gm_surf.nodes.node_coord.shape[0]),
                    wm_center_distance=np.ones(self.gm_surf.nodes.node_coord.shape[0]) * 0.5
            ).value

        # still, the comparison criterion must be fairly relaxed due to the randomness in the results... -.-
        assert np.allclose(self.reference_solutions["gradient_gm_wm"], np.mean(gradients_computed, axis=0), atol=1)

    def test_e_field_angle_theta(self):
        # perform 100 computations and compare their mean gain st the reference solution as
        # the involved 'interpolate_scattered' of SimNIBS produces non-consistent results.
        # (ie. results with some randomness, which becomes worse the coarser the mesh is)
        theta_computed = np.zeros((1000, 16))
        for i in range(1000):
            theta_computed[i, :] = pynibs.e_field_angle_theta(self.midlayer_surf, self.volmesh).value

        # still, the comparison criterion must be fairly relaxed due to the randomness in the results... -.-
        assert np.allclose(self.reference_solutions["efield_angle_theta"], np.mean(theta_computed, axis=0), atol=1)

    '''
    def precompute_geo_info_for_layer_field_interpolation(self):
        pass

    def test_calc_e_in_midlayer_roi(self):
        qois = ['E', 'mag', 'norm', 'tan', 'theta', 'gradient']

        roi_surf = pynibs.roi.RegionOfInterestSurface()
        roi_surf.node_number_list = self.midlayer_surf.elm.node_number_list[:,:3] - 1
        roi_surf.node_coord_mid = self.midlayer_surf.nodes.node_coord
        roi_surf.n_tris = self.volmesh.elm.triangles.shape[0]

        qois_in_midlayer = pynibs.calc_e_in_midlayer_roi(
            phi_dadt=(self.volmesh.elmdata[0].value, self.volmesh.nodedata[0].value),
            roi=roi_surf,
            subject=None,
            phi_scaling = 1.,
            mesh_idx = 0,
            mesh = self.volmesh,
            roi_hem = 'lh',
            depth = .5,
            qoi = qois,
            gm_nodes=self.gm['nodes'],
            wm_nodes=self.wm['nodes'],
            gm_center_distance=self.gm['dist'],
            wm_center_distance=self.wm['dist']
        )

                    # atol=1e1: results are rather large numbers, so the unit positions are not as vital
        assert np.allclose(self.reference_solutions["qois_in_midlayer"], qois_in_midlayer, atol=1e1)


    def init_surfaces(self):
        roi_nodes = self.midlayer_surf.nodes.node_coord

        _, closest_gm_indices = self.gm_surf.nodes.find_closest_node(roi_nodes, return_index=True)
        _, closest_wm_indices = self.wm_surf.nodes.find_closest_node(roi_nodes, return_index=True)

        # move a bit away from the boundary surface inside the gray matter
        # to avoid numercial glitches due to the tissue interface
        gray_matter_nodes_in_roi = self.gm_surf.nodes[closest_gm_indices] - \
                                   self.gm_surf.nodes_normals()[closest_gm_indices] * 0.001
        white_matter_nodes_in_roi = self.wm_surf.nodes[closest_wm_indices] + \
                                    self.wm_surf.nodes_normals()[closest_wm_indices] * 0.001

        gm_node_distance = np.linalg.norm(roi_nodes - gray_matter_nodes_in_roi, ord=2, axis=1)
        wm_node_distance = np.linalg.norm(roi_nodes - white_matter_nodes_in_roi, ord=2, axis=1)

        return gray_matter_nodes_in_roi, gm_node_distance, white_matter_nodes_in_roi, wm_node_distance
    '''
