import unittest
import os
import numpy as np
import pynibs


class TestNeuron(unittest.TestCase):

    def __init__(self, arg):
        super().__init__(arg)

        self.ref_solutions = {
            "E_eff_theta_grad_biph": np.array([
                [108.99286613, 85.96408903, 124.17195412, 98.8275486, 73.3491116],
                [144.53038703, 106.75814922, 111.11770803, 72.57034409, 86.49080839]
            ]),
            "E_eff_theta_grad_moph": np.array([
                [143.34643585, 87.9513034, 173.05044413, 120.4003543, 99.45095857],
                [206.68140871, 138.77622406, 147.85136069, 97.51548151, 88.60251602]
            ]),
            "E_sens_theta_grad_biph": np.array([
                [0.91749124, 1.16327645, 0.80533483, 1.01186361, 1.36334303],
                [0.69189602, 0.93669664, 0.89994657, 1.37797335, 1.15619222]
            ]),
            "E_sens_theta_grad_moph": np.array([
                [0.69761065, 1.13699281, 0.57786619, 0.83056234, 1.00552073],
                [0.48383645, 0.72058453, 0.67635495, 1.0254782 , 1.12863612]
            ]),
            "E_eff_theta_biph": np.array([
                [91.64035799, 89.76030555, 93.12729774, 89.67499275, 98.36438768],
                [96.70780296, 91.26493028, 91.95306402, 97.59416169, 89.50477012]
            ]),
            "E_eff_theta_moph": np.array([
                [104.58503273, 90.66017569, 111.4853239, 99.22654096, 96.74941352],
                [117.11759464, 103.46147386, 105.67433877, 95.01271821, 90.9427257]
            ]),
            "E_sens_theta_biph": np.array([
                [1.09122228, 1.1140782 , 1.07379901, 1.11513809, 1.01662809],
                [1.03404272, 1.09571113, 1.08751134, 1.02465146, 1.11725889]
            ]),
            "E_sens_theta_moph": np.array([
                [0.69761065, 1.13699281, 0.57786619, 0.83056234, 1.00552073],
                [0.48383645, 0.72058453, 0.67635495, 1.0254782 , 1.12863612]
            ])
        }
        self.test_data = {
            "theta": np.array([
                [139.31208874, 78.99811916, 154.54762558, 125.52624523, 16.95192262],
                [175.61202329, 137.00514636, 141.49157495, 23.06045388, 81.06946882]
            ]),
            "gradient": np.array([
                [54.79120971, -12.22431205, 71.71958398, 39.47360581, -81.16453042],
                [95.12447033, 52.2279404, 57.21286106, -74.37727346, -9.92281242]
            ]),
            "mag_e": np.ones((2, 5)) * 100
        }

    def test_calc_e_sensitivity(self):
        e_sens = pynibs.calc_e_threshold(
            "L23",
            self.test_data["theta"],
            self.test_data["gradient"],
            neuronmodel="sensitivity_weighting",
            waveform="biphasic"
        )
        assert np.allclose(
            e_sens,
            self.ref_solutions['E_sens_theta_grad_biph']
        ), "Computed result does not match reference result when using 'theta' and 'gradient' (biphasic)!"

        e_sens = pynibs.calc_e_threshold(
            "L23",
            self.test_data["theta"],
            self.test_data["gradient"],
            neuronmodel="sensitivity_weighting",
            waveform="monophasic"
        )
        assert np.allclose(
            e_sens,
            self.ref_solutions['E_sens_theta_grad_moph'],
        ), "Computed result does not match reference result when using 'theta' and 'gradient' (monophasic)!"

        e_sens = pynibs.calc_e_threshold(
            "L23",
            self.test_data["theta"],
            neuronmodel="sensitivity_weighting",
            waveform="biphasic"
        )
        assert np.allclose(
            e_sens,
            self.ref_solutions['E_sens_theta_biph']
        ), "Computed result does not match reference result when using 'theta' only (biphasic)!"

        e_sens = pynibs.calc_e_threshold(
            "L23",
            self.test_data["theta"],
            self.test_data["gradient"],
            neuronmodel="sensitivity_weighting",
            waveform="monophasic"
        )
        assert np.allclose(
            e_sens,
            self.ref_solutions['E_sens_theta_moph']
        ), "Computed result does not match reference result when using 'theta' only (monophasic)!"

    def test_calc_e_effective(self):
        e_eff = pynibs.calc_e_effective(
            self.test_data["mag_e"],
            "L23",
            self.test_data["theta"],
            self.test_data["gradient"],
            neuronmodel="sensitivity_weighting",
            waveform="biphasic"
        )
        assert np.allclose(
            e_eff,
            self.ref_solutions['E_eff_theta_grad_biph']
        ), "Computed result does not match reference result when using 'theta' and 'gradient' (biphasic)!"

        e_eff = pynibs.calc_e_effective(
            self.test_data["mag_e"],
            "L23",
            self.test_data["theta"],
            self.test_data["gradient"],
            neuronmodel="sensitivity_weighting",
            waveform="monophasic"
        )
        assert np.allclose(
            e_eff,
            self.ref_solutions['E_eff_theta_grad_moph']
        ), "Computed result does not match reference result when using 'theta' and 'gradient' (monophasic)!"

        e_eff = pynibs.calc_e_effective(
            self.test_data["mag_e"],
            "L23",
            self.test_data["theta"],
            neuronmodel="sensitivity_weighting",
            waveform="biphasic"
        )
        assert np.allclose(
                e_eff,
                self.ref_solutions['E_eff_theta_biph']
        ), "Computed result does not match reference result when using 'theta' only! (biphasic)"

        e_eff = pynibs.calc_e_effective(
            self.test_data["mag_e"],
            "L23",
            self.test_data["theta"],
            neuronmodel="sensitivity_weighting",
            waveform="monophasic"
        )
        assert np.allclose(
                e_eff,
                self.ref_solutions['E_eff_theta_moph']
        ), "Computed result does not match reference result when using 'theta' only! (monophasic)"

    def test_load_cell_model(self):
        models_folder = os.path.join(pynibs.__datadir__, "neuron", "models")

        models = {
            "L23": os.path.join(models_folder, "L23_PC_cADpyr_biphasic_v1.csv"),
            "L5": os.path.join(models_folder, "L5_TTPC2_cADpyr_biphasic_v1.csv")
        }

        for fn in models.values():
            interp, thetas, rel_gradients = pynibs.load_cell_model(str(fn))

            assert interp is not None and thetas is not None and rel_gradients is not None,\
                "Cannot load cell meanfield models. Do the responsibel CSV files still exist?"
